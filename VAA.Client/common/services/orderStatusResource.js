﻿(function () {
    "use strict";

    angular
    .module("common.services")
    .factory("orderStatusResource",
            ["$resource", "appSettings", orderStatusResource])


    function orderStatusResource($resource, appSettings) {
        console.log(appSettings.serverPath + "api/OrderStatus/");
        return $resource(appSettings.serverPath + "api/OrderStatus/");
    }

}());