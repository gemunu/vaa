﻿(function () {
    "use strict";

    angular
    .module("common.services")
    .factory("vegetableResource",
            ["$resource", "appSettings", vegetableResource])


    function vegetableResource($resource, appSettings) {
        console.log(appSettings.serverPath + "api/Vegetable/");
        return $resource(appSettings.serverPath + "api/Vegetable/");
    }

}());