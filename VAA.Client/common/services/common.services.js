﻿(function () {
    "use strict";

    var app = angular.module("common.services", ["ngResource"])
    .constant("appSettings",
    {
        //TODO: replace this URL with production server URL 
        //before deployment //vgauctionapi.scm.azurewebsites.net:443
        //serverPath: "http://localhost:27119/" //Dev
        serverPath: "http://vgauctionapi.azurewebsites.net/"
    })
    .constant("userSettings",
    {
        greetingMessage: "Welcome",
        mainRoute: "/Order"

    });




}());