﻿(function () {
    "use strict";
    
    angular
    .module("common.services")
    .factory("orderResource",
            ["$resource", "appSettings", orderResource])

    function orderResource($resource, appSettings) {
        console.log(appSettings.serverPath + "api/Order/");
        return $resource(appSettings.serverPath + "api/Order/");
    }


}());