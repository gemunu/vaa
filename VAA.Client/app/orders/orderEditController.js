(function () {
    "use strict";

    angular
        .module("vegetableAuction")
        .controller("OrderEditController",
                    ["orderResource", "vegetableResource", "$stateParams", OrderEditController]);

    function OrderEditController(orderResource, vegetableResource, $stateParams) {
        var vm = this;//need for $scope is gone this with
        vm.order = {};
        vm.message = '';

        vegetableResource.query(function (data) {
            vm.vegetables = data;
        });

        orderResource.get({ id: $stateParams.orderId },
            function (data) {
                vm.order = data;
                vm.originalOrder = angular.copy(data);
            });

        if (vm.order && vm.order.id) {
            vm.title = "Edit order with # " + vm.order.id;
        }
        else {
            vm.title = "New Order";
        }

        vm.submit = function () {
            vm.message = '';
            if (vm.order.id) {
                vm.order.$save({ id: vm.order.id },
                    function (data) {
                        vm.message = "... Update Complete";
                    })
            }
            else {
                vm.order.$save(
                    function (data) {
                        vm.originalOrder = angular.copy(data);

                        vm.message = "... Create Complete";
                    })
            }
        };

        vm.cancel = function (editForm) {
            editForm.$setPristine();
            vm.order = angular.copy(vm.originalOrder);
            vm.message = "";
        };

        vm.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            vm.opened = !vm.opened;
        }
    }
}());
