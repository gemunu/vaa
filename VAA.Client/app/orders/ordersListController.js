(function () {
    "use strict";
    angular
        .module("vegetableAuction")//main module in app.js
        .controller("OrdersListController",//registers itself with main module
                     ["orderResource", "orderStatusResource", "$location", OrdersListController]);

    
    function OrdersListController(orderResource, orderStatusResource, $location) {
        var vm = this; 
        //taking advantage of the controller as syntax
        //with this syntax we don't need to pass $scope to controller
        
        orderStatusResource.query(function (data) {
            vm.filterOptions = data;
            vm.searchCriteria = vm.filterOptions[0].name;
        });

        //Pick all the orders  by default
        orderResource.query(function (data) {
            vm.orders = data;
        });
        
        //Search selection change handler
        vm.searchOrders = function () {
            //Query parameter is given here as a name value pair
            orderResource.query({ search: vm.searchCriteria.name }, function (data) {
                vm.orders = data;
            });
        }

        //Edit click Action handler
        vm.editOrder = function (orderid) {
            $location.path("/orderEdit/" + orderid);
            //$state.go('orderEdit({orderId:'+orderid+'})')
        }

        vm.showImage = true;

        vm.toggleImage = function () {
            vm.showImage = !vm.showImage;
        }


    }

}());
