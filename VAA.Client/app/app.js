(function () {//self executing anonymous function
    "use strict";
    //With strict mode, you can not, for example, use 
    //undeclared variables.

    //create the module
    var app = angular.module("vegetableAuction",
                            ["ui.router",
                             "ngRoute",
                             "ui.bootstrap",
                             "common.services"]);

    //Setup routing
    //app.config(function ($routeProvider) {
    //    $routeProvider
    //      .when("/", {
    //          templateUrl: "app/Login.html",
    //          controller: "HomeController as vm"
    //      })
    //    .when("/orderList", {
    //        templateUrl: "app/orders/ordersListView.html",
    //        controller: "OrdersListController as vm"
    //    })
    //      .when("/orderEdit/:orderId", { //colon in route means parameter
    //          //this part is extracted from URL and given to other components
    //          templateUrl: "app/orders/orderEditView.html",
    //          controller: "OrderEditController as vm"
    //        })
    //      .otherwise({
    //          redirectTo: "/"
    //      });
    //});

    //Setup routing
    app.config(["$stateProvider","$urlRouterProvider",
                 function ($stateProvider, $urlRouterProvider) {

                     $urlRouterProvider.otherwise("/"); //this means orderList state activation

                     $stateProvider
                     //Orders list
                     .state("home", {
                         url: "/", //if we type #/orders in the brower after app url, we should get the list
                         templateUrl: "app/login.html",
                         controller: "LoginController as vm"
                     })
                     .state("orderList", {
                         url: "/orders", //if we type #/orders in the brower after app url, we should get the list
                         templateUrl: "app/orders/ordersListView.html",
                         controller: "OrdersListController as vm"
                     })
                     .state("orderEdit", {
                         url: "/orderEdit/:orderId", //if we type #/orderEdit in the brower after app url, we should get the list
                         templateUrl: "app/orders/orderEditView.html",
                         controller: "OrderEditController as vm"
                     })


                 }]
       );
 
}());