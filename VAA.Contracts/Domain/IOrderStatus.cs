﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VAA.Contracts.Domain
{
    public interface IOrderStatus
    {
        int Id { get; set; }
        string Name { get; set; }
      //  List<IOrder> Orders { get; set; }
    }
}
