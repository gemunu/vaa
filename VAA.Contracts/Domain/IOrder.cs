﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VAA.Contracts.Domain
{
    public interface IOrder
    {
        int Id { get; set; }
        int VegetableId { get; set; }
        int Quantity { get; set; }
        int OrderStatusId { get; set; }
        decimal ExpectedUnitPrice { get; set; }
        DateTime DueDate { get; set; }
        int? TargetSellerId { get; set; }
        //IOrderStatus OrderStatus { get; set; }
        //IVegetable Vegetable { get; set; }

    }
}
