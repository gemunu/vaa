﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VAA.Contracts.Domain;

namespace VAA.WebAPI.Models.Domain
{
    public class Order : IOrder
    {
        public DateTime DueDate { get; set; }

        public decimal ExpectedUnitPrice { get; set; }

        public int Id { get; set; }

        public int Quantity { get; set; }

        public int OrderStatusId { get; set; }

        public int? TargetSellerId { get; set; }

        public int VegetableId { get; set; }

        public OrderStatus OrderStatus { get; set; }
        public virtual Vegetable Vegetable { get; set; }

    }
}