﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VAA.Contracts.Domain;

namespace VAA.WebAPI.Models.Domain
{
    public class OrderStatus : IOrderStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}