namespace VAA.WebAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DueDate = c.DateTime(nullable: false),
                        ExpectedUnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Int(nullable: false),
                        OrderStatusId = c.Int(nullable: false),
                        TargetSellerId = c.Int(),
                        VegetableId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderStatus", t => t.OrderStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Vegetable", t => t.VegetableId, cascadeDelete: true)
                .Index(t => t.OrderStatusId)
                .Index(t => t.VegetableId);
            
            CreateTable(
                "dbo.OrderStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vegetable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "VegetableId", "dbo.Vegetable");
            DropForeignKey("dbo.Order", "OrderStatusId", "dbo.OrderStatus");
            DropIndex("dbo.Order", new[] { "VegetableId" });
            DropIndex("dbo.Order", new[] { "OrderStatusId" });
            DropTable("dbo.Vegetable");
            DropTable("dbo.OrderStatus");
            DropTable("dbo.Order");
        }
    }
}
