namespace VAA.WebAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using VAA.WebAPI.Context;
    using VAA.WebAPI.Models.Domain;

    internal sealed class Configuration : DbMigrationsConfiguration<VAA.WebAPI.Context.VAAConnection>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VAAConnection context)
        {
            //  This method will be called after migrating to the latest version.

            //Add initial data to tables
            context.Vegetables.AddOrUpdate(
                  v => v.Name,
                  new Vegetable { Name = "Bitter Gourd" },
                  new Vegetable { Name = "Baby Jack" },
                  new Vegetable { Name = "Beans" },
                  new Vegetable { Name = "Capsicum Chilies" },
                  new Vegetable { Name = "Ladies Fingers" },
                  new Vegetable { Name = "Drum Stick" },
                  new Vegetable { Name = "Onion Flower" },
                  new Vegetable { Name = "Long Beans" },
                  new Vegetable { Name = "Snake Gourd" },
                  new Vegetable { Name = "Pumpkin" },
                  new Vegetable { Name = "Wing Beans" },
                  new Vegetable { Name = "Chillies-Green" },
                  new Vegetable { Name = "Chillies-Red" },
                  new Vegetable { Name = "Squash" }
                );

            context.OrderStatus.AddOrUpdate(
                  os => os.Name,
                  new OrderStatus { Name = "Open to Bid" },
                  new OrderStatus { Name = "Cancelled" },
                  new OrderStatus { Name = "Supplied" }
                );

            //Run this only after making sure above block is run separately
            AddMockOrders(context);

        }

        private void AddMockOrders(VAAConnection context)
        {
            //Insert some mock orders
            context.Orders.AddOrUpdate(
                  o => new
                  {
                      o.VegetableId,
                      o.Quantity,
                      o.ExpectedUnitPrice,
                      o.DueDate,
                      o.OrderStatusId
                  },
                  new Order
                  {
                      VegetableId = 1,
                      Quantity = 25,
                      ExpectedUnitPrice = 100,
                      DueDate = new DateTime(2016, 01, 19),
                      OrderStatusId = 1
                  },
                  new Order
                  {
                      VegetableId = 2,
                      Quantity = 45,
                      ExpectedUnitPrice = 90,
                      DueDate = new DateTime(2015, 12, 10),
                      OrderStatusId = 3
                  },
                  new Order
                  {
                      VegetableId = 3,
                      Quantity = 50,
                      ExpectedUnitPrice = 200,
                      DueDate = new DateTime(2015, 11, 11),
                      OrderStatusId = 1
                  },
                  new Order
                  {
                      VegetableId = 4,
                      Quantity = 33,
                      ExpectedUnitPrice = 180,
                      DueDate = new DateTime(2015, 10, 30),
                      OrderStatusId = 2
                  },
                  new Order
                  {
                      VegetableId = 5,
                      Quantity = 25,
                      ExpectedUnitPrice = 110,
                      DueDate = new DateTime(2015, 12, 15),
                      OrderStatusId = 1
                  }
                );

        }
    }
}
