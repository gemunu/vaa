namespace VAA.WebAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageContentColumntoVegetableTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vegetable", "ImageContent", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vegetable", "ImageContent");
        }
    }
}
