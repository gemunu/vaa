﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using VAA.WebAPI.Models.Domain;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace VAA.WebAPI.Context
{
    public class VAAConnection : DbContext
    {
        public VAAConnection()
            : base("VAAConnection")
        {

        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<Vegetable> Vegetables { get; set; }



        //Removed table name pluralizing which is default in EF 6.
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}