﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VAA.WebAPI.Context;
using VAA.WebAPI.Models.Domain;

namespace VAA.WebAPI.Controllers
{
    //TODO : update this URL when deploying
    //[EnableCorsAttribute("http://localhost:27269", "*", "*")] //Dev
    [EnableCors("http://vgauction.azurewebsites.net", "*", "*")] //
    public class VegetableController : ApiController
    {
        private VAAConnection db = new VAAConnection();

        // GET: api/Vegetable
        //public IQueryable<Vegetable> GetVegetables()
        //{
        //    return db.Vegetables;
        //}

        public dynamic GetVegetables()
        {
            // return db.OrderStatus;
            var vegetables =
                    from v in db.Vegetables
                    select new
                    {
                        Id = v.Id,
                        Name = v.Name,
                        Image = v.ImageContent
                    };

            return vegetables;
        }

        //private void UploadImage(string path)
        //{
        //    // Retrieve storage account from connection string.
        //    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
        //        ConfigurationManager.ConnectionStrings["BlobConnection"].ConnectionString);

        //    // Create the blob client.
        //    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

        //    // Retrieve reference to a previously created container.
        //    CloudBlobContainer container = blobClient.GetContainerReference("vegetableImages");

        //    // Retrieve reference to a blob named "vegeblob".
        //    CloudBlockBlob blockBlob = container.GetBlockBlobReference("vegeblob");

        //    // Create or overwrite the "myblob" blob with contents from a local file.
        //    using (var fileStream = System.IO.File.OpenRead(path))
        //    {
        //        blockBlob.UploadFromStream(fileStream);
        //    }
        //}

        // GET: api/Vegetable/5
        [ResponseType(typeof(Vegetable))]
        public IHttpActionResult GetVegetable(int id)
        {
            Vegetable vegetable = db.Vegetables.Find(id);
            if (vegetable == null)
            {
                return NotFound();
            }

            return Ok(vegetable);
        }

        // PUT: api/Vegetable/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVegetable(int id, Vegetable vegetable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vegetable.Id)
            {
                return BadRequest();
            }

            db.Entry(vegetable).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VegetableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vegetable
        [ResponseType(typeof(Vegetable))]
        public IHttpActionResult PostVegetable(Vegetable vegetable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vegetables.Add(vegetable);
            db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = vegetable.Id }, vegetable);
        }

        // DELETE: api/Vegetable/5
        [ResponseType(typeof(Vegetable))]
        public IHttpActionResult DeleteVegetable(int id)
        {
            Vegetable vegetable = db.Vegetables.Find(id);
            if (vegetable == null)
            {
                return NotFound();
            }

            db.Vegetables.Remove(vegetable);
            db.SaveChanges();

            return Ok(vegetable);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VegetableExists(int id)
        {
            return db.Vegetables.Count(e => e.Id == id) > 0;
        }
    }
}