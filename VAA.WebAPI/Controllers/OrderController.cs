﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VAA.WebAPI.Context;
using VAA.WebAPI.Models.Domain;

namespace VAA.WebAPI.Controllers
{
    //TODO : update this URL when deploying
   // [EnableCorsAttribute("http://localhost:27269", "*", "*")] //Dev
    [EnableCors("http://vgauction.azurewebsites.net", "*", "*")]
    public class OrderController : ApiController
    {
        private VAAConnection db = new VAAConnection();

        // GET: api/Order
        //public IQueryable<Order> GetOrders()
        //{
        //    return db.Orders;

        //}

        // GET: api/Order
        public dynamic GetOrders()
        {
            var orders =
                from o in db.Orders
                select new
                {
                    Id = o.Id,
                    Vegetable = o.Vegetable.Name,
                    Image = o.Vegetable.ImageContent,
                    Quantity = o.Quantity,
                    ExpectedUnitPrice = o.ExpectedUnitPrice,
                    DueDate = o.DueDate,
                    Status = o.OrderStatus.Name
                };

            return orders;
        }

        // GET: api/Order
        public dynamic GetOrders(string search)
        {
            var orders =
                from o in db.Orders
                where o.OrderStatus.Name.Contains(search)
                select new
                {
                    Id = o.Id,
                    Vegetable = o.Vegetable.Name,
                    Image = o.Vegetable.ImageContent,
                    Quantity = o.Quantity,
                    ExpectedUnitPrice = o.ExpectedUnitPrice,
                    DueDate = o.DueDate,
                    Status = o.OrderStatus.Name
                };

            return orders;
        }

        // GET: api/Order/5
        //[ResponseType(typeof(Order))]
        //public IHttpActionResult GetOrder(int id)
        //{
        //    Order order = db.Orders.Find(id);
        //    if (order == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(order);
        //}

        public dynamic GetOrder(int id)
        {
            Order order;
                
            if(id>0)
                order  = db.Orders.Find(id);
            else
                order = db.Orders.Create();

            return new
            {
                Id = order.Id,
                VegetableId = order.VegetableId,
                Quantity = order.Quantity,
                ExpectedUnitPrice = order.ExpectedUnitPrice,
                DueDate = order.DueDate,
                Status = order.OrderStatusId
            };

            
        }

        //PUT: api/Order/5
        public void PutOrder(int id, [FromBody]Order order)
        {

            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();
           
        }

        //create
        // PUT: api/Order/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutOrder(int id, Order order)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != order.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(order).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!OrderExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Order
        //[ResponseType(typeof(Order))]
        //public IHttpActionResult PostOrder(Order order)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Orders.Add(order);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = order.Id }, order);
        //}
        //create
        // POST: api/Order
        public void PostOrder([FromBody]Order order)
        {
            //status always open to bid when creating new
            order.OrderStatusId = 1;

            db.Orders.Add(order);
            db.SaveChanges();
        }

        //modify
        public void PostOrder(int id, [FromBody]Order order)
        {
            //status always open to bid when saving
            order.OrderStatusId = 1;
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();
        }

        // DELETE: api/Order/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult DeleteOrder(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }

            db.Orders.Remove(order);
            db.SaveChanges();

            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.Id == id) > 0;
        }
    }
}